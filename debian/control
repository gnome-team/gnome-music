Source: gnome-music
Section: gnome
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Iain Lane <laney@debian.org>, Jeremy Bícha <jbicha@ubuntu.com>, Laurent Bigonville <bigon@debian.org>, Tim Lunn <tim@feathertop.org>
Build-Depends: debhelper-compat (= 13),
               appstream-util <!nocheck>,
               desktop-file-utils,
               dh-sequence-gir,
               dh-sequence-python3,
               grilo-plugins-0.3 (>= 0.3.16-1.1~),
               itstool,
               libadwaita-1-dev (>= 1.6),
               libgdk-pixbuf2.0-bin,
               libgirepository1.0-dev (>= 1.35.9),
               libglib2.0-dev (>= 2.67.1),
               libgoa-1.0-dev (>= 3.35.90),
               libgrilo-0.3-dev (>= 0.3.15-1.1~),
               libgtk-4-dev (>= 4.16.0),
               libmediaart-2.0-dev (>= 1.9.1),
               libpango1.0-dev (>= 1.44.0),
               librsvg2-common:native,
               libtracker-sparql-3.0-dev (>= 3.3.3),
               meson (>= 0.59.0),
               pkgconf,
               python-gi-dev (>= 3.50.0),
               python3 (>= 3.7),
               python3-cairo-dev (>= 1.14.0)
Rules-Requires-Root: no
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/gnome-team/gnome-music
Vcs-Git: https://salsa.debian.org/gnome-team/gnome-music.git
Homepage: https://apps.gnome.org/Music/

Package: gnome-music
Architecture: any
Depends: gir1.2-adw-1 (>= 1.6),
         gir1.2-gdkpixbuf-2.0,
         gir1.2-gio-2.0,
         gir1.2-girepository-2.0,
         gir1.2-glib-2.0 (>= 1.35.9),
         gir1.2-goa-1.0 (>= 3.35.90),
         gir1.2-gobject-2.0,
         gir1.2-grilo-0.3 (>= 0.3.15-1.1~),
         gir1.2-gst-plugins-base-1.0,
         gir1.2-gstreamer-1.0,
         gir1.2-gtk-4.0 (>= 4.16.0),
         gir1.2-mediaart-2.0 (>= 1.9.1),
         gir1.2-pango-1.0 (>= 1.44.0),
         gir1.2-soup-3.0,
         gir1.2-totemplparser-1.0,
         gir1.2-tracker-3.0,
         gnome-settings-daemon,
         grilo-plugins-0.3 (>= 0.3.13-3~),
         python3 (>= 3.7),
         python3-gi (>= 3.50.0),
         python3-gi-cairo,
         python3-requests,
         tracker (>= 3.0),
         ${gir:Depends},
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Suggests: gnome-online-accounts
Description: Music playing app for the GNOME Desktop
 Music is a music playing application that is a simple and elegant
 replacement for using Files to show the Music directory.
